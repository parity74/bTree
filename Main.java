// bTree by Justin Wong
//
// To the extent possible under law, the person who associated CC0 with
// bTree has waived all copyright and related or neighboring rights
// to bTree.
//
// You should have received a copy of the CC0 legalcode along with this
// work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

package bTree;

import java.util.*;

class Main{    
    public static void main(String[] args){
	//Create Random
	Random random = new Random();
	int curInt = random.nextInt(100);
	//Create node with value of 50
	Node node50 = new Node(50);
	//Insert 5 nodes,
	for(int i=0; i<5; i++){
	    curInt = random.nextInt(90)+10;
	    node50.addChild(new Node(curInt));
	}
	//Create root node, inserting 50 at random position
	Node root = new Node(curInt);
	int index = random.nextInt(3);
	for(int i=0; i<5; i++){
	    if(i == index)
		root.addChild(node50);
	    curInt = random.nextInt(90)+10;
	    System.out.println("Added " + curInt);
	    root.addChild(new Node(curInt));
	}
	
	//Print as tree for debugging
	System.out.println();
	root.print(0);
	//Print tree in single line
	System.out.println();
	root.print();
	System.out.println();

	//Test searching
	int target = 50;
	System.out.println();
	System.out.println("Searching for " + target);
	Node targetNode = root.search(target);
	System.out.println("Found " + targetNode);
	
	//Test deletion on target
	System.out.println();
	System.out.println("Killing node " + targetNode);
	System.out.println();
	targetNode.delete();
	//Print tree again, after deletion
	root.print(0);
	//Print tree in single line
	System.out.println();
	root.print();
	System.out.println();
    }
}
