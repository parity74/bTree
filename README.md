# bTree

Demonstration of simple binary tree.

Includes methods for printing, searching, insertion, and deletion.

Compile with

```
javac bTree/*.java
```

Run with

```
java bTree.Main
```
