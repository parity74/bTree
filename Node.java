// bTree by Justin Wong
//
// To the extent possible under law, the person who associated CC0 with
// bTree has waived all copyright and related or neighboring rights
// to bTree.
//
// You should have received a copy of the CC0 legalcode along with this
// work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

package bTree;

import java.util.*;

class Node{
    int data;
    Node parent;
    Node l;
    Node r;
    /**
     * Class Constructor
     */
    public Node(int data){
	this.data = data;
	this.parent = null;
	this.l = null;
	this.r = null;
    }
    /**
     * Add a child to this node
     * Preform recusive call if child is not null
     */
    public void addChild(Node node){
	if(node.data < this.data){
	    //Insert into left
	    if(this.l == null){
		this.l = node;
		node.parent = this;
	    }
	    else{
		this.l.addChild(node);
	    }
	}
	else{
	    //Insert right
	    if(this.r == null){
		this.r = node;
		node.parent = this;
	    }
	    else{
		this.r.addChild(node);
	    }
	}
    }
    /**
     * Recursive binary search
     */
    public Node search(int target){
	System.out.println("Search at " + this.data + " for " + target);
	//Compare with self
	if(this.data == target){
	    //Return self if match
	    return this;
	}
	else if(target > this.data && this.r != null){
	    //Recursive call on right child if not null 
	    return(this.r.search(target));
	}
	else if(target < this.data && this.l != null){
	    //Recursive call on left child if not null 
	    return(this.l.search(target));
	}
	//Return null if no children
	return null;
    }
    /**
     * Delete this node
     */
    public void delete(){
	//Edge case when deleting root
	if(parent == null){
	    l.addChild(r);
	    return;
	}
	if(this.l != null && this.r != null){
	    //Do DFS to find least node of right child
	    Node curNode = this.r;
	    while(curNode.l != null)
		curNode = curNode.l;
	    //Delete that lesser node
	    curNode.delete();
	    //Re-insert in this place
	    if(this == parent.l) parent.l = curNode;
	    if(this == parent.r) parent.r = curNode;
	    curNode.l = this.l;
	    curNode.r = this.r;
	}
	else if(this.r != null){
	    //Replace this with right child
	    if(this == parent.l) parent.l = this.r;
	    if(this == parent.r) parent.r = this.r;
	}
	else if(this.l != null){
	    //Replace this with left child
	    if(this == parent.l) parent.l = this.l;
	    if(this == parent.r) parent.r = this.l;
	}
	else{
	    //Remove if leaf
	    if(this == parent.l) parent.l = null;
	    if(this == parent.r) parent.r = null;
	}
    }
    /**
     * Print tree with indentation
     */
    public void print(int depth){
	//Print all lesser nodes
	if(this.l != null)
	    this.l.print(depth + 1);
	//Print self
	//add indentation an symbol to describe children
	for(int i=0; i<depth; i++)
	    System.out.print("    ");
	if(l == null && r == null)
	    System.out.println(this.data);
	else if(r == null) System.out.println(this.data + " /");
	else if(l == null) System.out.println(this.data + " \\");
	else System.out.println(this.data + " <");
	//Print all greater nodes
	if(this.r != null)
	    this.r.print(depth + 1);
    }
    /**
     * Print tree without indentation
     * Result is always sorted!
     */
    public void print(){
	//Print all lesser nodes
	if(this.l != null)
	    this.l.print();
	//Print self
	System.out.print(this.data + ", ");
	//Print all greater nodes
	if(this.r != null)
	    this.r.print();
    }
    /**
     * Override toString
     */
    @Override public String toString(){
	return(Integer.toString(data));
    }
}
